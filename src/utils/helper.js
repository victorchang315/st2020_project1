
let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    if (Number(a['PM2.5']) > Number(b['PM2.5'])) {
        return 1;
    }
    if (Number(a['PM2.5']) < Number(b['PM2.5'])) {
        return -1;
    }
    return 0;
}

let average = (nums) => {
    const result = nums.reduce((prev, cur) => prev + cur, 0)/nums.length;
    return +(Math.round(result + "e+2") + "e-2");
}


module.exports = {
    sorting,
    compare,
    average
}