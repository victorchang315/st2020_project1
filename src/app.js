import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import '@instructure/canvas-theme'
import { Grid } from '@instructure/ui-layout'
import { Heading } from '@instructure/ui-heading'
import { getPM25 } from './utils/pm25req';
import Chart from './components/Chart'


const App = () => {

  const [samples, setSamples] = useState('')

  const city = ['臺北市', '臺中市', '高雄市'];

  useEffect(() => {
    getPM25(city).then((result) => {
      setSamples(result);
    });    
  }, []);


  return (
    <div className="App">
      <Grid startAt="large" > {/* visualDebug */}
        <Grid.Row>
          <Grid.Col></Grid.Col>
          <Grid.Col width={8}>
            {/***************************************** */}
            <Heading>Project 1 - Gitlab CI, pipeline & unit test</Heading>
            <p>PM2.5 open data</p>
            
          </Grid.Col>
          <Grid.Col></Grid.Col>
        </Grid.Row>
      </Grid>
      
      <br/>

      <Grid startAt="large" >
        <Grid.Row>
          <Grid.Col></Grid.Col>
          <Grid.Col width={8}>
            {/***************************************** */}
            <a href="https://opendata.epa.gov.tw/Data/Contents/AQI/" target="_blank">行政院環境保護署。環境資源資料開放平臺</a><br/>
            Publish time: {typeof samples === "object" ? samples[0]['PublishTime'] : ""}
            <hr/>
            </Grid.Col>
          <Grid.Col></Grid.Col>
        </Grid.Row>
      </Grid>
        
      <br/>
        
      <Grid startAt="large" >
        <Grid.Row>
          <Grid.Col></Grid.Col>
          <Grid.Col width={8}>
            {/***************************************** */}
            <Chart samples={samples}/>

          </Grid.Col>
          <Grid.Col></Grid.Col>
        </Grid.Row>
      </Grid>

      <br/>

      
    </div>
  );
}


ReactDOM.render(<App />, document.getElementById('app'));