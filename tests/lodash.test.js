const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

test('array size', () => {
    const array = [1, 2, 3];
    let arrSize = _.size(array);
    expect(arrSize).to.eql(3);
})

test('array max element', () => {
    const array = [4, 2, 8, 6];
    let arrMax = _.max(array);
    expect(arrMax).to.eql(8);
})

test('array min element', () => {
    const array = [4, 2, 8, 6];
    let arrMin = _.min(array);
    expect(arrMin).to.eql(2);
})

test('add', () => {
    let res = _.add(6,4);
    expect(res).to.eql(10);
})

test('multiply', () => {
    let res = _.multiply(6,4);
    expect(res).to.eql(24);
})

test('divide', () => {
    let res = _.divide(6,4);
    expect(res).to.eql(1.5);
})

test('sum of array', () => {
    const array = [4, 2, 8, 6];
    let arraySum = _.sum(array);
    expect(arraySum).to.eql(20);
})

test('mean of array', () => {
    const array = [4, 2, 8, 6];
    let arrayMean = _.mean(array);
    expect(arrayMean).to.eql(5);
})

test('head of array', () => {
    const array = [4, 2, 8, 6];
    let arrayHead = _.head(array);
    expect(arrayHead).to.eql(4);
})

test('last of array', () => {
    const array = [4, 2, 8, 6];
    let arrayLast = _.last(array);
    expect(arrayLast).to.eql(6);
})